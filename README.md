# DnfMonster

#### 介绍
装备系统与nut联动方式扩展改动，普雷装备权能写法进行重写更新，修复bug
110副本采用新版本血量机制。难度改动
血量设定倍率
普通0.5
冒险1.0
王者5.0
地狱20.0
伤害设定倍率
普通1.0
冒险1.0
王者1.5
地狱2.0

刃影部分技能改动和bug修复


nut部分文件结构变更。后续模式功能的更新将放入 sqr/common/* 内便于统计管理
//common function import

sq_RunScript("common/2021.12.4_charm/main.nut");
sq_RunScript("common/2021.12.4_damagefont/class.nut");
sq_RunScript("common/2021.12.4_damagefont/damagenumber.nut");
sq_RunScript("common/2021.12.4_bullettime/bullet_time.nut");
sq_RunScript("common/2021.12.4_font/china_font.nut");
sq_RunScript("common/2021.12.4_font/fontlist.nut");
sq_RunScript("common/2021.12.4_setspeed/setspeed.nut");
sq_RunScript("common/2021.12.4_file/file.nut");
sq_RunScript("common/2021.12.4_information/main.nut");
sq_RunScript("common/2021.12.4_information/main_def.nut");
sq_RunScript("common/2021.12.4_thread/thread.nut");
sq_RunScript("common/2021.12.4_appendageclient/append.nut");
sq_RunScript("common/2021.12.4-ablitilyincrease/ablitilyincrease.nut");
sq_RunScript("common/2021.12.4_comboeffect/comboeffect.nut");
sq_RunScript("common/2021.12.4_comboeffect/commoneffectani.nut");
sq_RunScript("common/2021.12.4_the_hard_dgn/main.nut");
sq_RunScript("common/2021.12.4_preyequipment/main.nut");
sq_RunScript("common/2021.12.4_anton_dpl/main.nut");
sq_RunScript("common/2021.12.19_the_oculus/main.nut");
sq_RunScript("common/2022.1.13_homing/homing.nut");
sq_RunScript("common/2021.2.13_preload/preload.nut");
sq_RunScript("common/2022.3.17_dfo_town_executiation/main.nut");
sq_RunScript("common/2022.3.17_dfo_town_executiation/action_function.nut");
sq_RunScript("common/2022.3.29_debug/debug.nut");
sq_RunScript("common/2022.3.29_skilleffect/main.nut");
sq_RunScript("common/2022.4.4_areamove/areamove.nut");
sq_RunScript("common/2022.4.5_windows_main_ui/mainui.nut");
sq_RunScript("common/2022.4.5_windows_main_ui/class.nut");
sq_RunScript("common/2022.4.5_windows_main_ui/enum_vector.nut");
sq_RunScript("common/2022.4.5_windows_main_ui/sample/teleport.nut");
sq_RunScript("common/2022.4.5_windows_main_ui/sample/portmove.nut");
sq_RunScript("common/2022.4.5_windows_main_ui/sample/airship_rear.nut");
sq_RunScript("common/2022.4.5_windows_main_ui/sample/siroco_raid.nut");
sq_RunScript("common/2022.4.6_poongintrainingroom/main_def.nut");
sq_RunScript("common/2022.4.6_poongintrainingroom/poongintrainingroom.nut");
sq_RunScript("common/2022.4.6_dungeonevent/event.nut");
sq_RunScript("common/2022.4.12_setanglespeed/setanglespeed.nut");
sq_RunScript("common/2022.4.26_siroco_raid/main.nut");
sq_RunScript("common/2022.4.26_siroco_raid/drawcustom.nut");
sq_RunScript("common/2022.4.26_siroco_raid/class.nut");
sq_RunScript("common/2022.4.27_houseevent/main.nut");
sq_RunScript("common/2022.4.27_eventswitch/main.nut");
sq_RunScript("common/2022.5.1_lost_land/dungeonclear.nut");
sq_RunScript("common/2022.5.5_homing_ex/homing_ex.nut");
sq_RunScript("common/2022.5.22_setdummyobj/setdummyobj.nut");
sq_RunScript("common/2022.6.15_element/main.nut");
sq_RunScript("common/2022.7.21_setroll/main.nut");
sq_RunScript("common/2022.8.4_equipment/equipmentlist.nut");
sq_RunScript("character/common/camera/proc.nut");
sq_RunScript("common/summon_monster.nut");
sq_RunScript("common/playsoundwithtimeout.nut");
sq_RunScript("common/new_ui_.nut");
sq_RunScript("common/act_function.nut");
sq_RunScript("common/is_land.nut");
sq_RunScript("common/setcurrentposinmoveable.nu");
sq_RunScript("common/math.nut");
sq_RunScript("common/stl.nut");
sq_RunScript("common/etc/dnf_enum_header2.nut");

//end common function import


怪物方面统一放入sqr/monster/package/* 文件夹下统一更新便于统计管理
sqr/monster/package/main.nut进行注册

sq_RunScript("monster/package/super_damage_force/super_damage_force_character.nut");
sq_RunScript("monster/package/jump/jump.nut");
sq_RunScript("monster/package/force_hold/force_hold.nut");
sq_RunScript("monster/package/commonwarncircle/commonwarncircle.nut");
sq_RunScript("monster/package/commonwarnline/commonwarnline.nut");
sq_RunScript("monster/package/move/move.nut");
sq_RunScript("monster/package/global_monster/global_monster.nut");
sq_RunScript("monster/package/move_unlimit/move_unlimit.nut");
sq_RunScript("monster/package/notice/notice.nut");
sq_RunScript("monster/package/applyfollowmap/applyfollowmaptoplay.nut");
sq_RunScript("monster/package/casting_blue/casting.nut");
sq_RunScript("monster/package/customui_circle/customui_circle.nut");
sq_RunScript("monster/package/blood_gauge/blood_gauge.nut");
sq_RunScript("monster/package/summon_auxo/auxo.nut");
sq_RunScript("monster/package/set_global_flag/set_global_flag.nut");
sq_RunScript("monster/package/ignore_white/ignore_white.nut");
sq_RunScript("monster/package/rage/rage.nut");
sq_RunScript("monster/package/set_muliting_hp/set_muliting_hp.nut");
sq_RunScript("monster/package/on_tayberrs/on_tayberrs.nut");
sq_RunScript("monster/package/internation_accel/internation_accel.nut");
sq_RunScript("monster/package/internation_parabola/nternation_parabola.nut");
sq_RunScript("monster/package/casting_blue_only/casting_blue_only.nut");
sq_RunScript("monster/package/flashscreen/flashscreen.nut");
sq_RunScript("monster/package/playanimation/playanimation.nut");
sq_RunScript("monster/package/setpause/setpause.nut");
sq_RunScript("monster/package/superdamage/superdamage.nut");
//..*

sq_RunScript("monster/package/internation_accel_time/internation_accel_time.nut");
sq_RunScript("monster/package/internation_accel_time_ignoremap/internation_accel_time_ignoremap.nut");
sq_RunScript("monster/package/setmaxhp/setmaxhp.nut");
sq_RunScript("monster/package/setspin/setspin.nut");
sq_RunScript("monster/package/appendstatus/appendstatus.nut");
sq_RunScript("monster/package/casting_red/casting_red.nut");
sq_RunScript("monster/package/setspeedtotarget/setspeedtotarget.nut");
sq_RunScript("monster/package/jumptotargetex/jumptotargetex.nut");
sq_RunScript("monster/package/setdirectiontotarget/setdirectiontotarget.nut");
sq_RunScript("monster/package/worldboss_append/worldappend.nut");
sq_RunScript("monster/package/changehptosetaction/changehptosetaction.nut");
sq_RunScript("monster/package/grabon/grabon.nut");
sq_RunScript("monster/package/graboff/graboff.nut");
sq_RunScript("monster/package/setscale/setscale.nut");
sq_RunScript("monster/package/hometotarget/hometotarget.nut");
sq_RunScript("monster/package/commonwarn/createwarn.nut");
sq_RunScript("monster/package/onbullettime/onbullettime.nut");
sq_RunScript("monster/package/apply_nocolas/apply_nocolas.nut");
sq_RunScript("monster/package/setoutworld/setunderworld.nut");
sq_RunScript("monster/package/setabsorthp/setabsorthp.nut");
sq_RunScript("monster/package/setparenttosummonmaster/setparenttosummonmaster.nut");


精简nut内容，删除多余的冗余数据。






